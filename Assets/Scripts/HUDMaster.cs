﻿using UnityEngine;
using UnityEngine.UI;

public class HUDMaster : MonoBehaviour
{
    public Text _currentLevelTextField;
    
    private GameMaster _gameMaster;

    private void Awake()
    {
        _gameMaster = FindObjectOfType<GameMaster>();
    }

    private void Start()
    {
        _currentLevelTextField.text = $"Level {_gameMaster.m_level + 1}";
    }
}
