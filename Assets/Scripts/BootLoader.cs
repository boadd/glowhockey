﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BootLoader : MonoBehaviour
{
    public string m_nextScene = "Level_0";

    private void Awake()
    {
        AnalyticsMaster.GaInit();
        SceneManager.LoadScene(m_nextScene);
    }
}
