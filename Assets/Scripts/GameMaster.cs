﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public int m_level;
    public int m_nextLevelId => m_level == LAST_LEVEL ? 0 : (m_level + 1);

    private const int LAST_LEVEL = 8;
    
    public void Win()
    {
        SceneManager.LoadScene($"Level_{m_nextLevelId}");
        AnalyticsMaster.LogEvent("LevelComplete", m_level + 1);
    }

    public void Lose()
    {
        SceneManager.LoadScene($"Level_{m_level}");
    }
    
}
