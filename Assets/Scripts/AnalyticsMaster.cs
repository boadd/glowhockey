﻿using GameAnalyticsSDK;

public static class AnalyticsMaster
{
    public static void GaInit()
    {
        GameAnalytics.Initialize();
    }


    public static void LogEvent(string eventName, float eventValue)
    {
        GameAnalytics.NewDesignEvent(eventName, eventValue);
    }
    
}
