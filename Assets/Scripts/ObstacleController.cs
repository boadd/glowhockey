﻿using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    private GameMaster _gameMaster;

    private void Awake()
    {
        _gameMaster = FindObjectOfType<GameMaster>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            _gameMaster.Lose();
        }
    }
}
