﻿using UnityEngine;
using Random = UnityEngine.Random;

public class GateController : MonoBehaviour
{
    private GameMaster _gameMaster;

    public float m_minXPosition = 0f;
    public float m_maxXPosition = 1f;
    

    private void Awake()
    {
        _gameMaster = FindObjectOfType<GameMaster>();
    }

    private void Start()
    {
        var currentPos = transform.position;
        currentPos.x = Random.Range(m_minXPosition, m_maxXPosition);
        transform.position = currentPos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            _gameMaster.Win();
        }
    }
}
