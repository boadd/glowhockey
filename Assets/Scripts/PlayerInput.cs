﻿using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Rigidbody m_puckObject;
    
    private Camera _mainCamera;

    private Vector3 _startTouchPosition;

    private const float MAX_POWER = 15f;


    private void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            _startTouchPosition = Input.mousePosition;
        } else if (Input.GetMouseButtonUp(0))
        {
            var direction = _startTouchPosition - Input.mousePosition;

            if (direction.sqrMagnitude > MAX_POWER * MAX_POWER)
            {
                direction = direction.normalized * MAX_POWER;
            }

            var forceDirection = new Vector3(direction.x, 0f, direction.y);
            m_puckObject.velocity = forceDirection;
        }
    }
    
}
